
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

$('.carousel').carousel({
interval: 4000
});

$("#modalRegistrar").on('show.bs.modal', function(){
    console.log("El modal se esta mostrando");
    $("#btnsuscribete").removeClass("btn-primary");
    $("#btnsuscribete").addClass("btn-light");
    $("#btnsuscribete").prop("disabled",true);
 });

 $("#modalRegistrar").on('shown.bs.modal', function(){
    console.log("El modal se mostró");
 });

 $("#modalRegistrar").on('hide.bs.modal', function(){
    console.log("El modal se esta ocultando");
    $("#btnsuscribete").removeClass("btn-light");
    $("#btnsuscribete").addClass("btn-primary");
    $("#btnsuscribete").prop("disabled",false);
  });

  $("#modalRegistrar").on('hidden.bs.modal', function(){
    console.log("El modal se oculto");
});